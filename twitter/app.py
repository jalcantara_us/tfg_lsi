import threading
import sys
import os
import socket

import redis
import json
import re
import string

from time import sleep
from src import twitter_stream, ioutils

import requests

REDIS_CHANNEL = os.getenv('REDIS_STREAM', 'input')
try:
    REDIS_HOSTNAME = socket.gethostbyname("redis")
except:
    REDIS_HOSTNAME = 'localhost'
REDIS_PASSWORD = os.getenv('REDIS_PASSWORD', 'thisisredis')
MAXLEN = os.getenv('MAXLEN', int(1e9))

class TwitterThread(threading.Thread):
    def __init__(self, q):
        threading.Thread.__init__(self)
        self.q = q
    def run(self):
        stream = twitter_stream.MyStreamer(queue=self.q)
        # trends = stream.twython.get_place_trends(id=766273)[-1]
        trends = stream.twython.get_place_trends(id=2459115)[-1]
        twython_trends = [e['name'] for e in trends['trends']]
        # twython_trends = ["happ", "glad", "enjoy"]
        logger.info("Starting Stream Statuses collecting from the trends: {}".format(twython_trends))
        try:
            stream.statuses.filter(track=', '.join(twython_trends), language='en')
        except requests.exceptions.ChunkedEncodingError as e:
            logger.exception("Thread error, restarting!")
            raise e

def init_redis():
    return redis.Redis(
        host=REDIS_HOSTNAME,
        password='thisisredis',
        decode_responses=True)

class RedisWriter():
    def __init__(self):
        self.redis = init_redis()

    def __call__(self, message, **kwargs):
        # TODO: Limpiar texto de tweeter
        message["text"] = re.sub(r"[^\x00-\x7F]+", " ", message["text"])
        return self.redis_write_to_stream(REDIS_CHANNEL, self.json_dump(message["text"]))

    def redis_write_to_stream(self, name, fields, maxlen=128):
        return self.redis.xadd(name=name, fields=fields, maxlen=maxlen)

    # def redis_read_from_streams(self, redis_dict, block=0, count=None):
    #     redis_data = self.redis.xread(redis_dict, block=block, count=count)
    #     return redis_data

    @staticmethod
    def json_dump(message, target=0, source="twitter"):
        return {"target": target,
                "source": source,
                "message": message}
def main():
    """
    Main function for the module
    """
    while True:
        try:
            redis_object = RedisWriter()
            # redis_pub_sub = redis_object.pubsub()

            q = {}
            logger.info("Starting python twitter stream thread")
            thread = TwitterThread(q=q)
            thread.setDaemon(True)
            thread.start()

            while True:
                sleep(5)
                if not thread.is_alive():
                    thread = TwitterThread(q=q)
                    thread.setDaemon(True)
                    thread.start()
                logger.info("Added tweets: {}".format(json.dumps(len(q))))
                for e in q.copy():
                    item = q.pop(e)
                    redis_item = {k: str(v) for k, v in item.items()}
                    redis_object(redis_item)
        except redis.exceptions.ConnectionError:
            del(redis_object)
            sleep(1)
            logger.exception("{}".format(sys.exc_info()[0]))
        except requests.exceptions.ChunkedEncodingError:
            del(redis_object)
            sleep(1)
            logger.exception("{}".format(sys.exc_info()[0]))
        except requests.exceptions.ChunkedEncodingError:
            sleep(1)
            logger.exception("{}".format(sys.exc_info()[0]))
        except:
            sleep(1)
            logger.exception("{}".format(sys.exc_info()[0]))



if __name__ == "__main__":
    ## Arguments for the module
    args = ioutils.arguments_init()


    ## Logger
    logger = ioutils.logger_init(log_level=args['log_level'], log_path=args['log_path'])
    while True:
        try:
            main()
        except KeyboardInterrupt:
            logger.info("Exiting")
