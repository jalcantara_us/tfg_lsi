import logging
import argparse
from pathlib import Path
import sys
import os

LOG_DEFAULT_PATH = "/var/log/python_twitter_stream.log"

## Arguments initialization for the module
# @return dict: arguments dict
def arguments_init():
    args_parser = argparse.ArgumentParser()

    args_parser.add_argument("-ll", "--log_level", default=1, type=int,
                             help="Level for the logger: 1: Debug, 2: Info, 3: Warning, 4: Error, 5: Critical",
                             choices=[1, 2, 3, 4, 5])
    args_parser.add_argument("-lp", "--log_path", default=LOG_DEFAULT_PATH,
                             help="Path to store log")

    return vars(args_parser.parse_args())


def logger_init(log_level, log_path):
    """
    Logger initialization for the module
    @param log_level level for the logger: 1: Debug, 2: Info, 3: Warning, 4: Error, 5: Critical"
    @param log_path path to store log file
    @return logging.Logger: logger object
    """
    handler = logging.StreamHandler(stream=sys.stdout)
    handler.setFormatter(logging.Formatter('%(asctime)s,%(msecs)d[%(levelname)s]:%(message)s'))
    handler.setLevel(logging.DEBUG)

    new_logger = logging.getLogger(Path(log_path).stem)
    new_logger.addHandler(handler)

    new_logger.setLevel(log_level * 10)
    return new_logger

## From http://code.activestate.com/recipes/134892-getch-like-unbuffered-character-reading-from-stdin/
class _Getch:
    """Gets a single character from standard input.  Does not echo to the screen."""
    def __init__(self):
        self.impl = _GetchUnix()

    def __call__(self): return self.impl()


class _GetchUnix:
    def __init__(self):
        import tty, sys, termios # import termios now or else you'll get the Unix version on the Mac

    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

