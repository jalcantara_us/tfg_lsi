from twython import TwythonStreamer, Twython
import csv
import json

# Create a class that inherits TwythonStreamer
class MyStreamer(TwythonStreamer):
    def __init__(self, queue, credentials_file="./files/twitter_credentials.json",
                 timeout=300, retry_count=None, retry_in=10, client_args=None, handlers=None, chunk_size=1):
        self.queue = queue
        self.credentials = self.load_credentials(credentials_file)
        self.writer = csv.writer(open(r'files/saved_tweets.csv', 'a'))
        self.twython = Twython(app_key=self.credentials['CONSUMER_KEY'], app_secret=self.credentials['CONSUMER_SECRET'],
                 oauth_token=self.credentials['ACCESS_TOKEN'], oauth_token_secret=self.credentials['ACCESS_SECRET'])
        super().__init__(app_key=self.credentials['CONSUMER_KEY'], app_secret=self.credentials['CONSUMER_SECRET'],
                 oauth_token=self.credentials['ACCESS_TOKEN'], oauth_token_secret=self.credentials['ACCESS_SECRET'],
                 timeout=timeout, retry_count=retry_count, retry_in=retry_in, client_args=client_args,
                 handlers=handlers, chunk_size=chunk_size)

    # Load credentials from json file
    def load_credentials(self, credentials_file):
        with open(credentials_file, "r") as file:
            return json.load(file)

    # Received data
    def on_success(self, data):
        # Collect tweets
        tweet_data = self.process_new_tweet(data)
        if not tweet_data: return 0
        try:
            self.queue[tweet_data['id']] = tweet_data['data']
            # self.save_to_csv(tweet_data['data']['text'])
        except TypeError:
            pass

    # Save each tweet to csv file
    def save_to_csv(self, tweet):
        self.writer.writerow(list(tweet.values()))


    def process_new_tweet(self, tweet):
        d = {'data':{}}
        if ('id' not in tweet) or 'retweeted_status' in tweet:
            return 0
        d['id'] = tweet['id']
        d['data']['id'] = tweet['id']
        d['data']['origin_tweet'] = tweet.get('retweeted_status', {'id': ''})['id']
        d['data']['hashtags'] = [hashtag['text'] for hashtag in tweet['entities']['hashtags']]
        d['data']['text'] = tweet['extended_tweet']['full_text'] if 'extended_tweet' in tweet else tweet['text']
        d['data']['user'] = tweet['user']['screen_name']
        d['data']['user_loc'] = tweet['user']['location']
        d['data']['created_at'] = tweet['created_at']
        d['data']['quoted_count'] = tweet.get('quoted_count', 0)
        d['data']['retweeted_count'] = tweet.get('retweet_count', 0)
        return d

    # def process_quote(self, tweet):
    #     quoted_status_id = tweet['quoted_status']['id']
    #     if quoted_status_id in self.queue:
    #         self.queue[quoted_status_id]['quoted_count'] += 1
    #     else:
    #         return self.process_new_tweet(tweet['quoted_status'])
    #
    # def process_retweet(self, tweet):
    #     retweeted_status_id = tweet['retweeted_status']['id']
    #     if retweeted_status_id in self.queue:
    #         self.queue[retweeted_status_id]['retweeted_count'] += 1
    #     else:
    #         return self.process_new_tweet(tweet['retweeted_status'])

