from transformers import BertModel, BertPreTrainedModel
import torch
from loguru import logger
class BertForMultiLabelSequenceClassification(BertPreTrainedModel):
    """BERT model for classification.
    This module is composed of the BERT model with a linear layer on top of
    the pooled output.
    """

    def __init__(self, config, labels=None, emotions=False):
        super(BertForMultiLabelSequenceClassification, self).__init__(config, labels=labels, emotions=emotions)
        self.num_labels = labels
        self.bert = BertModel(config)
        self.dropout = torch.nn.Dropout(config.hidden_dropout_prob)
        self.classifier = torch.nn.Linear(config.hidden_size, self.num_labels)
        self.apply(self._init_weights)
        self.emotions = emotions

    def forward(self, input_ids, token_type_ids=None, attention_mask=None, labels=None):
        _, pooled_output = self.bert(input_ids, token_type_ids, attention_mask)
        pooled_output = self.dropout(pooled_output)
        logits = self.classifier(pooled_output)

        if labels is not None and not self.emotions:
            loss_fct = torch.nn.CrossEntropyLoss()
            loss = loss_fct(logits, labels)
            return loss, logits
        if labels is not None and self.emotions:
            loss_fct = torch.nn.BCEWithLogitsLoss()
            loss = loss_fct(logits.view(-1, self.num_labels), labels.view(-1, self.num_labels))
            return loss, logits
        else:
            return logits

    def freeze_bert_encoder(self):
        for param in self.bert.parameters():
            param.requires_grad = False

    def unfreeze_bert_encoder(self):
        for param in self.bert.parameters():
            param.requires_grad = True



class NewModel(torch.nn.Module): #new model
    def __init__(self, pretrained_model, num_inputs, num_outputs):
        super(NewModel, self).__init__()
        self.num_inputs = num_inputs
        self.num_outputs = num_outputs
        self.pretrained_model = pretrained_model
        self.new_layer = torch.nn.Linear(self.num_inputs, self.num_outputs)
        self.dropout = torch.nn.Dropout(0.1)

    def forward(self, input_ids, labels):
        pooled_output = self.pretrained_model(input_ids)
        pooled_output = self.dropout(pooled_output)
        logits = self.new_layer(pooled_output)

        if labels is not None:
            loss_fct = torch.nn.CrossEntropyLoss()
            loss = loss_fct(logits, labels)
            return loss, logits
        else:
            return logits