import os
from time import time, sleep

import torch
from loguru import logger
from transformers import BertTokenizer

from tqdm import tqdm
import json
import redis
import socket
os.environ["CUDA_LAUNCH_BLOCKING"] = "1"
try:
    REDIS_HOSTNAME = socket.gethostbyname("redis")
except:
    REDIS_HOSTNAME = 'localhost'

# Operación interesante para normalizar x = x/x.sum(0).expand_as(x)
from model import BertForMultiLabelSequenceClassification, NewModel

def init_redis():
    return redis.Redis(
        host=REDIS_HOSTNAME,
        password='thisisredis',
        decode_responses=True)

def init_device():
    return torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
"""
Save:
torch.save(model.state_dict(), PATH)

Load:
model = TheModelClass(*args, **kwargs)
model.load_state_dict(torch.load(PATH))
model.eval()
"""


# def init_model(bert="bert-base-uncased"):
#     config = BertConfig.from_pretrained(bert)
#     config.num_labels = 5
#     model = BertForSequenceClassification.from_pretrained(bert, config=config)
#     model.to(device)
#     model.load_state_dict(torch.load("./model_bert", map_location=device))
#     return model

def init_model(model_name, num_labels, emotions, state=None):
    logger.info("Loading model: {}\nLabels: {}\nEmotions: {}\nState: {}".format(model_name, num_labels, emotions, state))
    model = BertForMultiLabelSequenceClassification.from_pretrained(model_name, labels=num_labels, emotions=emotions)
    model.to(device)

    logger.info(model)
    if state:
        model_state_dict = torch.load(state, map_location=device)
        model.load_state_dict(model_state_dict, strict=False)
    return model


def init_tokenizer(bert="bert-base-uncased"):
    return BertTokenizer.from_pretrained(bert)


# Proceso para leer de redis
class RedisReader():
    def __init__(self):
        self.redis = init_redis()
        self.sub = self.redis.pubsub()
        self.sub.subscribe('input')
        self.redis_dict = {'input': '0-0'}
        self.redis_read_from_streams()
    def __call__(self, block=2000, **kwargs):
        # return self.sub.get_message(ignore_subscribe_messages=True, timeout=timeout)
        return self.redis_read_from_streams(block=block)

    def pub(self, message):
        return self.redis.publish('output', message)

    def redis_write_to_stream(self, name, fields, maxlen=128):
        return self.redis.xadd(name=name, fields=fields, maxlen=maxlen)

    def redis_read_from_streams(self, block=2000, count=None):
        def flatten(l):
            return [item for sublist in l for item in sublist]
        redis_data = dict(self.redis.xread(self.redis_dict, block=block, count=count))
        for k in redis_data:
            self.redis_dict[k] = redis_data.get(k, '0-0')[-1][0]
        list_redis_data = [dict(redis_data.get(e)).values() for e in redis_data]
        return flatten(list_redis_data)

    @staticmethod
    def json_load(redis_item):
        return json.loads(redis_item)

    @staticmethod
    def json_dump(output_list, source=0, target='twitter'):
        return {"source": source,
                "target": target,
                "message": output_list}



# TODO: Clase para clasificar

from torch.utils.data import Dataset
class DatasetInit(Dataset):
    def __init__(self, sentence_list):
        self.data = [
            (
                self.rpad(
                    tokenizer.encode(sentence), n=128
                )
            )
            for sentence in sentence_list
        ]

    def rpad(self, array, n=64):
        """Right padding."""
        current_len = len(array)
        if current_len > n:
            return array[: n]
        extra = n - current_len
        return array + ([0] * extra)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        x = self.data[index]
        x = torch.tensor(x)
        return x

def main():
    vector_list = []

    while True:
        item_list = redis_reader()
        if not item_list:
            logger.debug("No message received from last 2 seconds")
            continue
        matches = [e['message'] for e in item_list if e['source'] == 'manual']
        if matches:
            logger.info("Matched")
            match = [matches[-1]]
            dataset = DatasetInit(match)
            generator = torch.utils.data.DataLoader(
                dataset, batch_size=1, shuffle=False
            )
            model.eval()
            with torch.no_grad():
                for batch in tqdm(generator):
                    batch = batch.to(device)
                    logits = model(batch, labels=None)
                    output = ' '.join([str(e) for e in logits.mean(0).to(torch.device('cpu')).detach().tolist()])
                    redis_reader.redis_write_to_stream('output', redis_reader.json_dump(output, source=NUM_LABELS, target='manual'))
                    # logger.info("Result: {}".format(logits))
        not_matched = [e['message'] for e in item_list if e['source'] != 'manual']
        if not not_matched:
            logger.info("Not matched")
            continue
        vector_list += not_matched
        if len(vector_list) < 32:
            continue
        batch_size = min(64, len(vector_list))
        dataset = DatasetInit(vector_list.copy())
        logger.info([e[:5] for e in vector_list])
        vector_list.clear()
        generator = torch.utils.data.DataLoader(
            dataset, batch_size=batch_size, shuffle=False
        )
        model.eval()
        with torch.no_grad():
            for batch in tqdm(generator):
                batch = batch.to(device)
                logits = model(batch, labels=None)
                output = ' '.join([str(e) for e in logits.mean(0).to(torch.device('cpu')).detach().tolist()])
                redis_reader.redis_write_to_stream('output', redis_reader.json_dump(output, source=NUM_LABELS, target='twitter'))
                # logger.info("Result: {}".format(logits))
        # logger.debug("New input: {}".format(item))


if __name__ == '__main__':
    logger.info("Starting Sentiment module")
    while True:
        try:
            redis_reader = RedisReader()
            logger.info("Redis connection started")

            logger.info("Model loading process started")
            device = init_device()
            model_name = os.environ.get("MODEL_NAME", "bert-base-cased")
            NUM_LABELS = int(os.environ.get("NUM_LABELS", 5))
            EMOTIONS = bool(int(os.environ.get("EMOTIONS", 0)))
            STATE = os.environ.get("STATE", "bert-base-cased_1598862628.5295944_2e_SGD_sst_50")
            model = init_model(model_name, num_labels=NUM_LABELS, emotions=EMOTIONS, state=STATE)
            tokenizer = BertTokenizer.from_pretrained(model_name)

            logger.info("Model loading success")

            logger.info("Starting main function")
            main()
        except KeyboardInterrupt:
            logger.info("Exiting")
            break
