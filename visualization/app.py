from argparse import ArgumentParser
import os
import socket
import sys

from loguru import logger
from time import sleep, time
import json
import redis

from flask import Flask, render_template, request, Response
from flask_restful import Api, Resource
from flask_cors import CORS

import numpy as np

try:
    REDIS_DEFAULT_IP = socket.gethostbyname('redis')
    WEB_HOST = os.getenv('WEB_HOST', '0.0.0.0')
except socket.gaierror:
    REDIS_DEFAULT_IP = 'localhost'
    WEB_HOST = 'localhost'
REDIS_DEFAULT_PORT = 6379
REDIS_DEFAULT_PASSWORD = ""


emotion_list = ['admiration', 'amusement', 'anger', 'annoyance', 'approval', 'caring', 'confusion', 'curiosity', 'desire', 'disappointment', 'disapproval', 'disgust', 'embarrassment', 'excitement', 'fear', 'gratitude', 'grief', 'joy', 'love', 'nervousness', 'optimism', 'pride', 'realization', 'relief', 'remorse', 'sadness', 'surprise', 'neutral']
emotion_select = ['joy', 'admiration', 'fear', 'surprise', 'sadness', 'disgust', 'anger', 'curiosity']

TEMPLATE = "/src/main_app/templates"


app = Flask(__name__, template_folder=TEMPLATE)
CORS(app)
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
app.config['UPDATE'] = False
api = Api(app)

def init_redis():
    return redis.Redis(
        host=REDIS_DEFAULT_IP,
        password='thisisredis',
        decode_responses=True)

# Proceso para leer de redis
class RedisReader():
    def __init__(self):
        self.redis = init_redis()
        self.sub = self.redis.pubsub()
        self.sub.subscribe('output')
        self.redis_dict = {'output': '0-0'}

    def __call__(self, block=5000, **kwargs):
        sleep(1)
        return self.redis_read_from_streams(block=block)

    def pub(self, message):
        return self.redis.publish('input', message)

    def redis_write_to_stream(self, name, fields, maxlen=128):
        return self.redis.xadd(name=name, fields=fields, maxlen=maxlen)

    def redis_read_from_streams(self, block=5000, count=None):
        def flatten(l):
            return [item for sublist in l for item in sublist]
        redis_data = dict(self.redis.xread(self.redis_dict, block=block, count=count))
        for k in redis_data:
            self.redis_dict[k] = redis_data.get(k, '0-0')[-1][0].replace('-0', '-1')
        list_redis_data = [dict(redis_data.get(e)).values() for e in redis_data]
        return flatten(list_redis_data)

    @staticmethod
    def redis_decode(redis_item):
        return json.loads(redis_item)

    @staticmethod
    def json_parsing(data):
        return json.dumps(data)


@app.route('/', methods=['GET'])
def init():
    from glob import glob
    logger.info(glob("**"))
    return render_template('index.html')


@app.route('/data-channel')
def data_channel():
    def parse_redis_requests():
        def normalizar(x):
            return x/sum(x)
        while True:
            try:
                item_list = redis_reader(5000)
                matches = [e for e in item_list if e['target'] in ['twitter', 'manual']]
                if not matches:
                    logger.debug("No message received from last 5 seconds")
                    continue
                for data in matches:
                    data["message"] = np.array([float(e) for e in data["message"].split(' ')])
                    if data["source"] == '28':
                        data["message"] = [data["message"][emotion_list.index(e)] for e in emotion_select]

                    data["message"] = list(normalizar(data["message"] - min(data["message"])))
                    json_output = redis_reader.json_parsing(data)
                    yield "event:response\ndata:{}\n\n".format(json_output)
            except KeyError as error:
                raise error
    return Response(parse_redis_requests(), mimetype='text/event-stream')

class timer():
    def __init__(self):
        self.timing = time()


class Petition(Resource):
    def __init__(self):
        self.dict = {'input': '0-0'}

    def post(self):
        data = request.get_json()
        redis_reader.redis_write_to_stream('input', data)
        return data, 200, {'ContentType': 'application/json'}

def main():
    api.add_resource(Petition, '/v1/input')

    app.run(debug=True, host=WEB_HOST, port=8000, threaded=True)


if __name__ == '__main__':
    logger.info("Starting Sentiment module")
    while True:
        try:

            redis_reader = RedisReader()
            logger.info("Redis connection started")

            logger.info("Starting main function")
            main()
        except KeyboardInterrupt:
            logger.info("Exiting")
            break