import pytest
import requests
import logging

logging.basicConfig(level=logging.INFO)
response_code = {'ok': 200,
                 'method_not_allowed': 405,
                 'conflict': 409,
                 'error': 500}

'''
@tag:           
@req:           
@text:          
@condition:     
@event:         
@expected:      
'''
@pytest.mark.parametrize("method, petition, expected_response", 
                            [
                                (requests.get, 'v1/ccds/crew/input/default/cabinzones/1/brt', response_code['method_not_allowed']),
                                (requests.post, 'v1/ccds/crew/input/default/cabinzones/1/brt', response_code['ok']),
                                (requests.post, 'v1/ccds/crew/input/default/cabinzones/1/unknow_function', response_code['conflict'])
                            ]
                        )
def test_crew_api(method, petition, expected_response):
    url = 'http://localhost:8000/{}'.format(petition)
    data = {'test_key': 'test_value'}
    response = method(url=url, data=data)
    assert response.status_code == expected_response

