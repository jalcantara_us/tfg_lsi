# Instrucciones de uso

Para lanzar el proyecto solo es necesario clonar el repositorio y lanzar los comandos:
```bash
docker-compose build
docker-compose up
```

# Requisitos

* Docker
* Docker-compose
* GPU con CUDA